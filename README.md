# manytab

Clean multiline tabs in Atom.

Tabs automatically size to file name.

Supports the [Atom][atom], [One][one], [Material][material], [Unity][unity],
[Nucleus][nucleus], and [Seti][seti] themes.

[atom]: https://atom.io/themes/atom-dark-ui
[one]: https://atom.io/themes/one-dark-ui
[material]: https://atom.io/themes/atom-material-ui
[unity]: https://atom.io/themes/unity-ui
[nucleus]: https://atom.io/themes/nucleus-dark-ui
[seti]: https://atom.io/themes/seti-ui

![Demonstration with Atom theme](img/atom.png "Atom theme")

![Demonstration with One theme](img/one.png "One theme")

![Demonstration with Material theme](img/material.png "Material theme")

![Demonstration with Unity theme](img/unity.png "Unity theme")

![Demonstration with Nucleus theme](img/nucleus.png "Nucleus theme")

![Demonstration with Seti theme](img/seti.png "Seti theme")
